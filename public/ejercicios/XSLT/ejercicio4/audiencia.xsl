<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="/programacio">
        <html>
            <head>
            </head>
            <body>
                <cadena>
                    <nom><xsl:value-of select="audiencia/cadenes/cadena[@nom='Un TV']/@nom"></xsl:value-of></nom>
                    <programas>
                        <xsl:apply-templates match="audiencia"/>
                    </programas>
                </cadena>
            </body>
        </html>


        
</xsl:template>

<!-- <xsl:template match="audiencia">
    <programa>
        <xsl:attribute name="hora">
            <xsl:value-of select="hora"/>
        </xsl:attribute>
        <xsl:for-each select="cadenes">
            <nom-programa>
                <xsl:value-of select="cadena[@nom='Un TV']"/>
            </nom-programa>
            <audiencia>
                <xsl:value-of select="cadena[@nom='Un TV']/@percentatge"/>
            </audiencia>
        </xsl:for-each>
    </programa>
</xsl:template> -->

<xsl:template match="audiencia">
    <programa>
        <xsl:attribute name="hora">
            <xsl:value-of select="hora"/>
        </xsl:attribute>
        <nom-programa>
            <xsl:value-of select="cadenes/cadena[@nom='Un TV']"/>
        </nom-programa>
        <audiencia>
            <xsl:value-of select="cadenes/cadena[@nom='Un TV']/@percentatge"/>
        </audiencia>
    </programa>
</xsl:template>

</xsl:stylesheet>
