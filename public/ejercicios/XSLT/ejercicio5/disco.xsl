<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/discos">
        <xsl:variable name="muse">
            <xsl:value-of select="group[@id='museId']/name"></xsl:value-of>
        </xsl:variable>
        <xsl:variable name="feeder">
            <xsl:value-of select="group[@id='feederId']/name"></xsl:value-of>
        </xsl:variable>
        <lista>
            <xsl:for-each select="disco">

                <disco>
                    <xsl:value-of select="title"/>
                es interpretado por
                    <xsl:if test="interpreter/@id='museId'">
                        <xsl:value-of select="$muse"/>
                    </xsl:if>
                    <xsl:if test="interpreter/@id='feederId'">
                        <xsl:value-of select="$feeder"/>
                    </xsl:if>
                </disco>
            </xsl:for-each>
        </lista>
    </xsl:template>
</xsl:stylesheet>
