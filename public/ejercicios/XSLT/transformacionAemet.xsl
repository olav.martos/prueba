<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="/">
   <html>
       <body>
            <table style="border: 1px solid black">
                <tr>
                    <th>Fecha</th>
                    <th>Maxima</th>
                    <th>Minima</th>
                    <th>Prediccion</th>
                </tr>
                <xsl:apply-templates/>
            </table>
       </body>
    </html>
</xsl:template>

<xsl:template match="root/prediccion/dia">
    <xsl:for-each select=".">
                    <tr>
                        <th><xsl:value-of select="@fecha"/></th>
                        <th><xsl:value-of select="temperatura/maxima"/></th>
                        <th><xsl:value-of select="temperatura/minima"/></th>
                        <th><img src="{concat('imagenesAemet/',estado_cielo/@descripcion)}.jpg" style="width:100px"/></th>
                    </tr>
                </xsl:for-each>
</xsl:template>
</xsl:stylesheet>