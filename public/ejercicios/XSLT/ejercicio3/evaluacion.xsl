<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <html>
            <body>
                <table style="border: 1px solid black">
                    <tr style="background-color:green">
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Telefono</th>
                        <th>Repetidor</th>
                        <th>Nota Practica</th>
                        <th>Nota Examen</th>
                        <th>Nota Total</th>
                    </tr>
                    <xsl:apply-templates match="evaluacion"/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="evaluacion">
        <xsl:for-each select="alumno">
            <xsl:sort select="apellidos" order="ascending"/>
            <tr>
                <th>
                    <img style="width:50px">
                        <xsl:choose>
                            <xsl:when test="@imagen">
                                <xsl:attribute name="src">fotos/alumnos/<xsl:value-of select="@imagen"/></xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="src">fotos/defecto.jpg</xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                    </img>
                </th>
                <th>
                    <xsl:value-of select="nombre"/>
                </th>
                <th>
                    <xsl:value-of select="apellidos"/>
                </th>
                <th>
                    <xsl:value-of select="telefono"/>
                </th>
                <th>
                    <xsl:value-of select="@repite"/>
                </th>
                <th>
                    <xsl:value-of select="notas/practicas"/>
                </th>
                <th>
                    <xsl:value-of select="notas/examen"/>
                </th>
                <!-- <xsl:if test="(notas/practicas+notas/examen)div2 &lt; 5">
                    <th style="color:red;"><xsl:value-of select="(notas/practicas+notas/examen)div2"/></th>
                </xsl:if>
                <xsl:if test="(notas/practicas+notas/examen)div2 &gt;= 8">
                    <th style="color:blue;"><xsl:value-of select="(notas/practicas+notas/examen)div2"/></th>
                </xsl:if>
                <xsl:if test="(notas/practicas+notas/examen)div2 &lt; 8 and (notas/practicas+notas/examen)div2 &gt;= 5">
                    <th style="color:black;"><xsl:value-of select="(notas/practicas+notas/examen)div2"/></th>
                </xsl:if> -->
                <xsl:choose>
                    <xsl:when test="(notas/practicas+notas/examen)div2 &lt; 5">
                        <th style="color:red;">
                            <xsl:value-of select="(notas/practicas+notas/examen)div2"/>
                        </th>
                    </xsl:when>
                    <xsl:when test="(notas/practicas+notas/examen)div2 &gt;= 8">
                        <th style="color:blue;">
                            <xsl:value-of select="(notas/practicas+notas/examen)div2"/>
                        </th>
                    </xsl:when>
                    <xsl:otherwise>
                        <th style="color:black;">
                            <xsl:value-of select="(notas/practicas+notas/examen)div2"/>
                        </th>
                    </xsl:otherwise>
                </xsl:choose>
            </tr>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
